package com.example.basestylecode.model

data class DataModel(val title: String?, val detail: String?)
